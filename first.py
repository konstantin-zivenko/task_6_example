from datetime import datetime, timedelta


class Record:
    def __init__(self, pilot: str = None, team: str = None, start: datetime | None = None, stop: datetime | None = None):
        self.pilot = pilot
        self.team = team
        self.start = start
        self.stop = stop


    @property
    def result(self) -> timedelta | None:
        if self.start and self.stop:
            return self.stop - self.start
        return None

    @property
    def start(self) -> datetime | None:
        return self._start

    @start.setter
    def start(self, value) -> None:
        if isinstance(value, datetime):
            if not self._stop or self._stop > value:
                self._start = value
        else:
            self._start = None

    @property
    def stop(self) -> datetime | None:
        return self._stop

    @stop.setter
    def stop(self, value) -> None:
        if isinstance(value, datetime):
            if not self._start or value > self._start:
                self._stop = value
        else:
            self._stop = None

    def __str__(self):
        return f"{self.pilot:<20} | {self.team:<25} | {self.result if self.result else "not result"}"

    def __repr__(self):
        return f"Record({self.pilot}, {self.team}, {self.start}, {self.stop})"


def get_data(path_file_abbrs, path_file_starts, path_file_ends) -> list[Record]:
    with open(path_file_abbrs) as abbrs, open(path_file_starts) as starts, open(path_file_ends) as ends:
        results = {}
        for full_abbr in abbrs:
            abbr, pilot, team = full_abbr.split("_")
            results[abbr] = Record(pilot=pilot, team=team[:-1])
        for start in starts:
            try:
                results[start[:3]].start = datetime.fromisoformat(start[3:-1])
            except ValueError:
                pass
        for stop in ends:
            try:
                results[stop[:3]].stop = datetime.fromisoformat(stop[3:-1])
            except ValueError:
                pass

        return list(results.values())


def build_report(row_results: list[Record], order: bool = True) -> list[Record]:
    results = []
    bad_results = []
    for result in row_results:
        if result.result:
            results.append(result)
        else:
            bad_results.append(result)
    if order:
        results.sort(key=lambda result: result.result)
    else:
        results.sort(key=lambda result: - result.result)
    return results + bad_results


def print_report(ordered_results: list[Record]) -> None:
    result_string = ""
    for num, record in enumerate(ordered_results, start=1):
        result_string += f"{num:<2}. {record.pilot:<20} | {record.team:<30} | {record.result}\n"
    print(result_string)


if __name__ == "__main__":
    path_file_abbrs = "data/abbreviations.txt"
    path_file_starts = "data/start.log"
    path_file_ends = "data/end.log"

    results = get_data(
        path_file_abbrs,
        path_file_starts,
        path_file_ends
    )

    results = build_report(results)

    print_report(results)
